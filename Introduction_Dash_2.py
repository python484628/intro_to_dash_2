
# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# October 2021

####################################### Second Introduction to Dash



#################### Import libraries
import dash 
from dash import dcc
from dash import html 
from dash.dependencies import Input, Output






############################################## First example of dashboard
# Access stylesheets
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
# Create the app
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# Create the layout
app.layout=html.Div([
    
    # Create a dropdown with 3 different labels New York City, Montreal, San Francisco
    html.Label('Dropdown'),
    dcc.Dropdown(
        options = [
            {'label': 'New York City', 'value' :'NYC'},
            {'label': 'Montreal', 'value' :'MTL'},
            {'label': 'San Francisco', 'value' :'SF'}
            ],
        value = "MTL"), # Default value when the app starts
        #multi=True),
    
    
    # Create a multi selection Dropdown (meaning that you can select multiple choices at the same time)
    html.Label('Multi-select Dropdown'),
    dcc.Dropdown(
        options= [
            {'label': 'New York City', 'value' :'NYC'},
            {'label': 'Montreal', 'value' :'MTL'},
            {'label': 'San Francisco', 'value' :'SF'}
            ],
        value = "MTL", # Default value when the app starts
        multi=True), # To have the multi selection possible
    
    # Create radio items with the same labels as before
    html.Label("Radio Items"),
    dcc.RadioItems(
        options= [
            {'label': 'New York City', 'value' :'NYC'},
            {'label': 'Montreal', 'value' :'MTL'},
            {'label': 'San Francisco', 'value' :'SF'}
            ],
        value = "MTL"
        ),
    
    # Create check boxes with the same labels as before
    html.Label("Check Box"),
    dcc.Checklist(
        options= [
            {'label': 'New York City', 'value' :'NYC'},
            {'label': 'Montreal', 'value' :'MTL'},
            {'label': 'San Francisco', 'value' :'SF'}
            ],
        value = ["MTL", "SF"]),
    
    
    # Create a text input
    html.Label("Text input"),
    dcc.Input(value="MTL", type="text"), # Default value when the app starts
    
    # Create a Slider between 0 and 9 with default value to 5 when the app starts
    html.Label("Slider"),
    dcc.Slider(
        min=0,
        max=9,
        value=5,
        marks={i:"Label{}".format(i) if i==1 else str(i) for i in range(1,6)}) # update of the displayed number
    
    
    
    
    ], style={"columnCount":2}) # 2 columns to spread all the previous options (slider, check boxes etc) on 2 columns/parts of the screen


# Run the app
if __name__ == '__main__':
    app.run_server(debug=False)
    
    
    
    








############################################## Second example of dashboard

# Create app
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# Create layout
app.layout=html.Div([
    dcc.Input(id='in1', value='initial value', type='text'),
    html.Div(id='out1')
    ])

# Create callback to display a specific output depending on the input that we give
# @ is a decorator, it's the fact to have a function inside another function
@app.callback(
    Output(component_id='out1', component_property='children'), # output (id from the layout)
    [Input(component_id='in1', component_property='value')] # input (id from the layout)
    )

# Create function that returns what the user is typing (text)
def my_callback1(input_value):
    return 'You\'ve entered "{}"'.format(input_value)


# Run the app
if __name__ == '__main__':
    app.run_server(debug=False)
    
    
    










############################################## Third example of dashboard   
    
# Create app
app = dash.Dash("test")

# Create layout
app.layout = html.Div([
    dcc.Input(id='in1', value='initial value 1', type = 'text'),
    html.Div(id='out1'),
    
    
    dcc.Input(id='in2', value='initial value 2', type = 'text'),
    html.Div(id='out2'),
              
              
              ])

# First callback
@app.callback(
    Output(component_id='out1', component_property='children'),
    [Input(component_id='in1', component_property='value')]
    )

# Function for first callback (same as the previous example)
def my_callback1(input_value):
    return 'You\'ve entered "{}"'.format(input_value)


# Second callback
@app.callback(
    Output(component_id='out2', component_property='children'),
    [Input(component_id='in2', component_property='value')]
    )

# Second function for second callback
def my_callback2(input_value):
    return 'You\'ve entered "{}"'.format(input_value)

# Run the app
if __name__ == '__main__':
    app.run_server(debug=False)















############################################## Fourth example of dashboard  

# Create app
app = dash.Dash("test")

# Create layout
app.layout = html.Div([
    html.H1('Example'),
    dcc.Input(
        id='my-text-input',
        value='Initial value'
        
        ),
    # Create Dropdown with 3 labels A,B,C and data
    dcc.Dropdown(
        id='my-dropdown',
        options=[
            {'label':'A', 'value':'[3,1,2]'}, 
            {'label':'B', 'value':'[4,3,5]'},
            {'label':'C', 'value':'[1,2,4]'}
            ],
        value='[3, 1, 2]' # Default value when the app starts
        ),
    # Create an empty graph
        dcc.Graph(id='my-graph')
    
    ])


# First callback to update the graph depending on the dropdown selected value
@app.callback(
    Output('my-graph', 'figure'), # Graph as output
    [
     Input('my-dropdown','value'), # Choice selection in the dropdown as input
     Input('my-text-input','value')] # Text as input
    
    )

# Create function to update the graph depending on the inputs
def update_graph(new_dropdown_value, new_text_input_value): # 2 parameters because 2 inputs above
    return{
        'data':[{
            'x':[1,2,3],
            'y':eval(new_dropdown_value)
            }],
        'layout': {'title':new_text_input_value} #We collect the title written by the user 
        
        }
    
# Run the app
if __name__ =='__main__':
    app.run_server(debug=False)

















########################################### Exercice to plot iris data and update graph with user selection


# Link that helped : https://towardsdatascience.com/interactive-visualization-with-dash-and-plotly-29eaccc90104

# Import libraries
import plotly.graph_objs as go
import random
import pandas as pd

# Open file iris
processed_files_path = "C:/Users/mario/downloads/"
names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
data= pd.read_csv(processed_files_path+"iris.data", sep=',', names=names)


# Create the app
app = dash.Dash("test")

# Create the layout
app.layout = html.Div(
    [
        html.Div([
            # Create First Dropdown with names as labels
            dcc.Dropdown(
                id='ddl_x',
                options=[{'label': i, 'value': i} for i in names],
                value='sepal-width',
                style={'width':'50%'}
            ),
            
            # Create second Dropdown with names as labels as well
            dcc.Dropdown(
                id='ddl_y',
                options=[{'label': i, 'value': i} for i in names],
                value='petal-width',
                style={'width':'50%'}
            ),
        ],style={'width':'100%','display':'inline-block'}),
        html.Div([
            # Create empty graph
            dcc.Graph(id='graph1') 
        ],style={'width':'100%','display':'inline-block'})
    ]
)

# Callback to update the graph depending on the user choices in the dropdowns
@app.callback(
    Output(component_id='graph1', component_property='figure'),
    [
        Input(component_id='ddl_x', component_property='value'),
        Input(component_id='ddl_y', component_property='value')
    ]
)

# Create function to update the graph
def update_output(ddl_x_value, ddl_y_value): # 2 parameters because 2 inputs above
    figure={
        'data': [
            # Create scatter plot with x and y that update depending on the user inputs in the dropdowns
            go.Scatter(
                x=data[data['class'] == cls][ddl_x_value],
                y=data[data['class'] == cls][ddl_y_value],
                mode='markers',
                marker={ 'size': 15 },
                name=cls
            ) for cls in data['class'].unique()
        ],
        # Graph layout
        'layout': 
            go.Layout(
                height= 600,
                hovermode= 'closest',
                title=go.layout.Title(text='Dash Interactive Data Visualization',xref='paper', x=0)
            )
        
    }
    return figure


# Run the app
if __name__ == '__main__':
    app.run_server(debug=False)





















